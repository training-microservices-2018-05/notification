package com.artivisi.training.microservice201805.notification.dto;

import lombok.Data;

@Data
public class EmailNotification {

    private String to;
    private String from;
    private String subject;
    private String content;

}
