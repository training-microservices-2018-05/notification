package com.artivisi.training.microservice201805.notification.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class KafkaSenderService {

    @Autowired private ObjectMapper objectMapper;
    @Autowired private KafkaTemplate<String, String> kafkaTemplate;

    @Value("${kafka.topic.notifikasi.response}")
    private String topicNotifikasiResponse;

    public void kirimStatus(String messageId, String status) {
        try {
            Map<String, String> hasilPengiriman = new HashMap<>();
            hasilPengiriman.put("status", status);
            hasilPengiriman.put("id", messageId);

            String json = objectMapper.writeValueAsString(hasilPengiriman);
            kafkaTemplate.send(topicNotifikasiResponse, json);
        } catch (Exception err) {
            err.printStackTrace();
        }
    }
}
